<?php

return [
    'name' => 'SessionDay',
    'manifest' => [
        'name' => env('APP_NAME', 'SessionDay'),
        'short_name' => 'SessiondDay',
        'start_url' => '/',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'scope'=>'./',
        'icons' => [
            '57x57' => '/images/icons/apple-icon-57x57.png',
            '60x60' => '/images/icons/apple-icon-60x60.png',
            '72x72' => '/images/icons/apple-icon-72x72.png',
            '76x76' => '/images/icons/apple-icon-76x76.png',
            '114x114' => '/images/icons/apple-icon-114x114.png',
            '120x120' => '/images/icons/apple-icon-120x120.png',
            '144x144' => '/images/icons/apple-icon-144x144.png',
            '152x152' => '/images/icons/apple-icon-152x152.png'
        ],
        // 'splash' => [
        //     '640x1136' => '/images/icons/apple-splash-640x1136.png',
        //     '750x1334' => '/images/icons/apple-splash-750x1334.png',
        //     '828x1792' => '/images/icons/apple-splash-828x1792.png',
        //     '1125x2436' => '/images/icons/apple-splash-1125x2436.png',
        //     '1242x2208' => '/images/icons/apple-splash-1242x2208.png',
        //     '1242x2688' => '/images/icons/apple-splash-1242x2688.png',
        //     '1536x2048' => '/images/icons/apple-splash-1536x2048.png',
        //     '1668x2224' => '/images/icons/apple-splash-1668x2224.png',
        //     '1668x2388' => '/images/icons/apple-splash-1668x2388.png',
        //     '2048x2732' => '/images/icons/apple-splash-2048x2732.png',
        // ],
        'custom' => []
    ]
];
