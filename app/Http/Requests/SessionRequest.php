<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_dj' => 'nullable|string ',
            'date_reproduction' => 'required|date ',
            'iframe_pc' => 'required|string ',
            'iframe_phone' => 'required|string '
        ];
    }

    public function messages()
    {
        return[
            'date_reproduction.required' => __('backend.sessions.form.date_reproduction.required'),
            'iframe_pc.required' => __('backend.sessions.form.iframe_pc.required'),
            'iframe_phone.required' => __('backend.sessions.form.iframe_phone.required'),
        ];
    }
}
