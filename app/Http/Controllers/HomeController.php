<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\HomeService;
use Illuminate\Console\Scheduling\Schedule;

class HomeController extends Controller
{    
    private $homeService;

    public function __construct(HomeService $homeService)
    {
        $this->homeService = $homeService;
    }
    
    public function index()
    {
        $session = $this->homeService->index();

        return view('pages.home', compact('session'));
    }

    public function lastSessions()
    {
        $values = $this->homeService->lastSessions();

        return view('pages.week', [$values['sessions'],$values['week']]);
    }
}
