<?php

namespace App\Http\Controllers\Mage;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\SessionService;
use App\Http\Controllers\Controller;
use App\Http\Requests\SessionRequest;

class SessionController extends Controller
{
    private $sessionsService;

    public function __construct(SessionService $sessionsService)
    {
        $this->sessionsService = $sessionsService;
    }

    public function index()
    {
        return view('pages.mage.sessions.index');
    }

    public function list()
    {
        return $this->sessionsService->list();
    }

    public function listPassed()
    {
        return $this->sessionsService->listPassed();
    }

    public function calendar()
    {
        return $this->sessionsService->calendar();
    }

    public function calendarEventId($id)
    {
        return $this->sessionsService->calendarEventId($id);
    }

    public function create()
    {
        return view('pages.mage.sessions.create');
    }

    public function store(SessionRequest $request)
    {
        $data = $request->validated();
        $this->sessionsService->store($data);

        return redirect(route('sessions.index'))->with('status', 'created');
    }

    public function edit($id)
    {
        $session = $this->sessionsServices->find($id);
        return view('pages.mage.sessions.edit', compact('session'));
    }

    public function update(SessionRequest $request, $id)
    {
        $data = $request->validated();
        $this->sessionsServices->update($data);

        return redirect(route('sessions.index'))->with('status', 'updated');
    }

    public function destroy($id)
    {
        $this->sessionsServices->delete($id);
    }
}
