<?php

namespace App\Services;

use Carbon\Carbon;
use App\Repositories\SessionsRepository;

class HomeService extends Service
{
    protected $sessions;

    /**
     * SessionsService constructor.
     */
    public function __construct(SessionsRepository $sessions)
    {
        $this->sessions = $sessions;
    }

    public function index()
    {
        $now = $this->now();
        return $this->sessions->todaySession($now->format('Y-m-d'));
    }

    public function lastSessions()
    {
        $now = $this->now();
        $sessions = $this->sessions->lastSessions($now->format('Y-m-d'));
        
        $week[0] = $now->isoFormat('dddd');
        for ($i=1; $i < 7; $i++) { 
            $week[$i] = $now->subDay(1)->isoFormat('dddd');
        }
        return array("sessions" => $sessions, "week" => $week);
    }

    public function now()
    {
        return Carbon::now('Europe/Madrid');
    }
}
