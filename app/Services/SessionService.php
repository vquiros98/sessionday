<?php

namespace App\Services;

use App\Repositories\SessionsRepository;

class SessionService extends Service
{
    protected $sessions;

    /**
     * SessionsService constructor.
     */
    public function __construct(SessionsRepository $sessions)
    {
        $this->sessions = $sessions;
    }

    public function list()
    {
        return $this->sessions->list();
    }

    public function listPassed()
    {
        return $this->sessions->listPassed();
    }

    public function calendar()
    {
        return response()->json($this->sessions->filterCalendar());
    }

    public function calendarEventId($id)
    {
        return response()->json($this->sessions->find($id));
    }

    public function store($data)
    {
        $data = $this->checkNameDj($data);

        $this->sessions->create($data);
    }

    public function find($id)
    {
        return $this->sessions->find($id);
    }

    public function update($data)
    {
        $data = $this->checkNameDj($data);

        $this->sessions->update($id,$data);
    }

    private function checkNameDj($data)
    {
        if($data['name_dj'] == null){
            $data['name_dj'] = 'Unknown';
        }
        return $data;
    }

    public function delete($id)
    {
        $this->sessions->delete($id);
    }
}
