<?php

namespace App\Services;

abstract class Service
{
    /**
     * Service constructor.
     */
    public function __construct()
    {
        //
    }


    /**
     * Returns all the repository registers
     * @return mixed
     */
    public function all()
    {
        return $this->repository->all();
    }


    /**
     * Returns all the repository registers in $ids
     * @param array $ids
     * @return mixed
     */
    public function getByIds(array $ids)
    {
        return $this->repository->findWhereIn('id', $ids);
    }


    /**
     * Returns all the repository registers in $id
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->repository->find($id);
    }
}
