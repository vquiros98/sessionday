<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'name_dj',
        'date_reproduction',
        'iframe_pc',
        'iframe_phone',
    ];
}
