<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Session;
use Omatech\Lars\BaseRepository;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class SessionsRepository extends BaseRepository
{
    /**
     * @return mixed
     */
    public function model() : String
    {
        return Session::class;
    }

    public function list()
    {
        $today = Carbon::now('Europe/Madrid')->format('Y-m-d');
        $sessions = $this->query()->where('date_reproduction', '>=', $today);

        return DataTables::of($sessions)->make(true);
    }

    public function listPassed()
    {
        $today = Carbon::now('Europe/Madrid')->format('Y-m-d');
        $sessions = $this->query()->where('date_reproduction', '<', $today)->orderBy('date_reproduction', 'desc');

        return DataTables::of($sessions)->make(true);
    }

    public function todaySession(string $date)
    {
        return $this->query()
            ->where('date_reproduction', '<=', $date)
            ->orderBy('date_reproduction', 'desc')
            ->first();
    }

    public function create($data)
    {
        $this->query()->create($data);
    }

    public function delete($id)
    {
        $this->query()
            ->where('id', $id)
            ->delete();
    }

    public function find($id)
    {
        return $this->query()
            ->where('id', $id)
            ->first();
    }

    function update($id, $data)
    {
        return $this->query()
            ->where('id', $id)
            ->update($data);
    }

    public function all()
    {
        return $this->query()
            ->get();
    }

    public function count()
    {
        return $this->query()
            ->count();
    }

    public function exists($id)
    {
        return $this->query()->where('id', $id)->exists();
    }

    public function where($where, $value)
    {
        return $this->query()->where($where, $value);
    }

    public function lastSessions($value)
    {
        return $this->query()->where('date_reproduction', '<=', $value)->orderBy('date_reproduction', 'desc')->take(7)->get();
    }

    public function filterCalendar()
    {        
        return $this->query()->get();
    }
}
