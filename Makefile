SHELL=/bin/bash

HOSTS_ENTRY:=127.0.0.1 local.sessionday.com
PLATFORM := $(shell uname -s)
KEYCLOAK_CONTAINER_ID = $(shell docker ps --filter name="$(stack_name)_keycloak" -q)


ifeq ($(PLATFORM),Darwin)
	export DOCKER_HOST_NAME_OR_IP := docker.for.mac.localhost
else ifeq ($(PLATFORM),Linux)
	export DOCKER_HOST_NAME_OR_IP := $(shell ip -f inet addr show docker0 | grep -Po 'inet \K[\d.]+')
else
	$(error Unable to automatically determine DOCKER_HOST_NAME_OR_IP: please provide it yourself by running: export DOCKER_HOST_NAME_OR_IP=...)
endif

COLOR_NC:=\033[0m
COLOR_WHITE:=\033[1;37m
COLOR_BLACK:=\033[0;30m
COLOR_BLUE:=\033[0;34m
COLOR_LIGHT_BLUE:=\033[1;34m
COLOR_GREEN:=\033[0;32m
COLOR_LIGHT_GREEN:=\033[1;32m
COLOR_CYAN:=\033[0;36m
COLOR_LIGHT_CYAN:=\033[1;36m
COLOR_RED:=\033[0;31m
COLOR_LIGHT_RED:=\033[1;31m
COLOR_PURPLE:=\033[0;35m
COLOR_LIGHT_PURPLE:=\033[1;35m
COLOR_BROWN:=\033[0;33m
COLOR_YELLOW:=\033[1;33m
COLOR_GRAY:=\033[0;30m
COLOR_LIGHT_GRAY:=\033[0;37m

.PHONY: help
help:
	@echo ""
	@echo -e "$(COLOR_GREEN)Make commands:$(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)help          	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) show this help $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)docker-build  	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) recreate new php images $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)hosts-entry   	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) add an entry for local-escript.zur_rose.com in etc/hosts $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)install       	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) set up docker and environment for testing and executing $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)dev           	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) boots docker environment $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)cli           	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) executes symfony console in a container $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)down          	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) down docker environment $(COLOR_NC)"
	@echo -e "    $(COLOR_YELLOW)kill          	       		$(COLOR_PURPLE)-> $(COLOR_CYAN) down and remove docker environment $(COLOR_NC)"
	@echo ""

## docker-build: build docker images
.PHONY: docker-build
docker-build:
	docker-compose build

## hosts-entry: Set up an entry for this project's host names in /etc/hosts
.PHONY: hosts-entry
hosts-entry:
	(grep "$(HOSTS_ENTRY)" /etc/hosts) || echo '$(HOSTS_ENTRY)' | sudo tee -a /etc/hosts

.PHONY: del-hosts-entry
del-hosts-entry:
	sudo sed -i".bak" "/$(HOSTS_ENTRY)/d" /etc/hosts

## up: Set up dev environment ready to work
.PHONY: install
install: docker-build dev

## dev: Boot dev environment
.PHONY: dev
dev: hosts-entry
	docker-compose up -d --no-build --remove-orphans --force-recreate
	docker-compose exec php-fpm composer install -o --prefer-dist
	docker-compose exec php-fpm php artisan migrate --force
	docker-compose exec php-fpm php artisan db:seed
	@echo "####################################################################"
	@echo ""
	@echo "Done, now open http://local.sessionday.com:8080 in your browser"
	@echo ""
	@echo "####################################################################"

## Run symfony cli command
.PHONY: cli
cli:
	docker-compose exec php-fpm ./artisan ${command}

## down: Stop containers for this project
.PHONY: stop
stop:
	docker-compose stop

## down: Stop and remove all containers and volumes for this project
.PHONY: down
down: del-hosts-entry
	docker-compose down --remove-orphans -v

## destroy: remove everything to be able to start all over
.PHONY: kill
kill: del-hosts-entry
	rm -rv vendor
	docker-compose down -v --remove-orphans --rmi all

# ## Run phpstan for static bug checking
# .PHONY: phpstan
# phpstan:
# 	@docker-compose exec php-fpm bin/phpstan analyse -l 5 --memory-limit=256M -c resources/code-assessment/phpstan/phpstan.neon.dist src tests
#
# ## test run unit tests
# .PHONY: test-unit
# test-unit:
# 	@docker-compose exec php-fpm bin/phpunit -c tests/Unit/phpunit.xml ${parameters}
#
# ## test run acceptance behat tests
# .PHONY: test-acceptance
# test-acceptance:
# 	@docker-compose exec php-fpm bin/behat -c behat.yml.dist --colors ${parameters}
#
# ## test run unit integration
# .PHONY: test-integration
# test-integration:
# 	@docker-compose exec php-fpm bin/phpunit -c tests/Integration/phpunit.xml ${parameters}
#
# # Check compromised php packages
# .PHONY: security-check
# security-check:
# 	@docker-compose exec php-fpm bin/console security:check

