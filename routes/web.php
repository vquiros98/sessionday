<?php

use Thujohn\Twitter\Facades\Twitter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/week', function () {
//     return view('week');
// });

Route::get('/lastSessions', 'HomeController@lastSessions')->name('lastSessions');
Route::resource('/', 'HomeController');
Route::get('sessions/{id}/addLike', 'Mage\SessionController@addLike')->name('backend.sessions.addLike');

Route::group(['middleware' => ['mageRedirectIfNotAuthenticated', 'setLocale']], function () {

    /*
    * SessionDay
    */
    Route::get('sessions/list', 'Mage\SessionController@list')->name('backend.sessions.list');
    Route::get('sessions/listPassed', 'Mage\SessionController@listPassed')->name('backend.sessions.listPassed');
    Route::get('sessions/calendar', 'Mage\SessionController@calendar')->name('backend.sessions.calendar');
    Route::get('sessions/calendar/{session}', 'Mage\SessionController@calendarEventId')->name('backend.sessions.calendarEventId');
    
    Route::resource('/backend/sessions', 'Mage\SessionController');

});