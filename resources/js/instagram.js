jQ(document).ready(function(){
    function apiInstagram()
    {
        jQ.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
            "meta": {
                "code": 200
            },
            data: {count: 5},
            url:"https://api.instagram.com/v1/users/self/media/recent/?access_token=35328985100.2a46ab3.750b5b5386184305b2acf072df07ca56",

            success: function(data) {
                
                instagram = JSON.stringify(data);
                instagramOBJ = jQ.parseJSON(instagram);
                console.log(instagramOBJ);
                instagramOBJ['data'].forEach(function(insta, index) {
                    jQ('#instagram').append('<div id="imgInsta'+index+'" data-id="'+index+'" data-tags="'+insta.tags+'" style=" width:110px; height:110px; margin:7px"><img alt="Imagen Instagram" width="100%" height="100%" src="'+insta.images.standard_resolution.url+'"></div>');
                    if(insta.type != 'video'){
                        jQ('#showImg').append('<div id="imgInstagram'+index+'" data-id="'+index+'"><img alt="Imagen Instagram" width="80%" src="'+insta.images.standard_resolution.url+'"></div>');
                    }else if(insta.type == 'video'){
                        jQ('#showImg').append('<div id="imgInstagram'+index+'" data-id="'+index+'"><video onloadstart="this.volume=0" width="80%" controls><source alt="Video Instagram" width="80%" src="'+insta.videos.standard_resolution.url+'" type="video/mp4"></video></div>');
                    }

                    for (i = 1; i < 5; i++) { 
                        jQ("#imgInstagram"+i).hide();
                    }

                    jQ("#imgInsta"+index).on('click',function() {
                        console.log(insta);
                        jQ("#instaCard").show();
                        jQ("#username h1").remove();
                        jQ('#username').append('<h1 style="font-weight:bold; font-size:18px; display: inline-block; margin: 0px";>'+insta.user.username+'</h1>'); 
                        jQ("#likes *").remove();
                        jQ('#likes').append('<h1 style="font-weight:bold; font-size:14px; display: inline-block; margin: 0px";>'+insta.likes.count+"&nbsp"+'</h1><i class="far fa-heart"></i>');
                                                                        
                        for (i = 0; i < 5; i++) { 
                            if(jQ(this).attr("data-id") == jQ('#imgInstagram'+i).attr("data-id")){
                                jQ("#imgInstagram"+i).show();   
                                jQ("#imgInstagram"+i).siblings().hide();
                            }
                        }
                    });     
                });
            },
            error: function(){
            console.log("Fallo");
            }
        });
    }
    apiInstagram();
})