import dayGridPlugin from '@fullcalendar/daygrid';

jQ(document).ready(function() {

  let calendarEl = document.getElementById('calendar');

  if(calendarEl !== undefined && calendarEl !== null) {
    var calendar = new Calendar.Calendar(calendarEl, {    
      plugins: [dayGridPlugin],
      header: {
        right: 'prev,next today',
        center: 'title',
        left: ''
      },
      locale: 'es',
      lang:'es',
      buttonText: {
        today: 'Hoy'
      },
      firstDay: 1,
      eventClick: function(info) {
        var eventObj = info.event;
        showInfoSessions(info);
      },
      eventSources: [
        {
          events:function(fetchInfo, successCallback, failureCallback) {
            jQ.ajax({
              type:'GET',
              url: route('backend.sessions.calendar'),
              data: {
              },
              success: function(response) {

                successCallback(response.map(function(event) {
                  return {
                    id:event.id,
                    title:event.name_dj,
                    start:event.date_reproduction,
                    end:event.date_reproduction,
                    color: "#11B6A4",
                    textColor: 'white'
                  }

                })
                );
              },
              error: function(response){
                console.log(response);
                failureCallback(response);
              },
            });
          }
        }
      ],      
    });
    calendar.render();
  };
  
  function showInfoSessions(info){
    let id = info.event.id;
    jQ.ajax({
        url: route('backend.sessions.calendarEventId', id),
        success: function(respuesta) {          
            console.log(respuesta);
            jQ('#sessionday_sessions_calendar_info h3').html(respuesta.name_dj)
            jQ('.iframeShow').html(respuesta.iframe_phone);

            if(respuesta.date_reproduction != null && respuesta.date_reproduction != null){
              jQ('#sessionday_sessions_calendar_info h4').html(respuesta.date_reproduction)
              jQ('#sessionday_sessions_calendar_info h4').append(" - " + respuesta.date_reproduction)
            }
            else{
              jQ('#sessionday_sessions_calendar_info h4').html("<b>La fecha no está definida</b>")
            }

            jQ('#sessionday_sessions_calendar_info h3').show()
            jQ('#sessionday_sessions_calendar_info h4').show()
            jQ('#sessionday_sessions_calendar_info h5').show()
        },
        error: function() {
            console.log(error);
        }
    });
  }

});