jQ(document).ready(function(){
    jQ(".date_session").flatpickr({
        enableTime: false,
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        locale: {
            firstDayOfWeek: 1
       }
    });
})