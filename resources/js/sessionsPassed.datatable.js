jQ(document).ready(function () {
    let $backendSessionsPassedDatatable = jQ('#backend-sessionsPassed-datatable');

    $backendSessionsPassedDatatable.DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: route('backend.sessions.listPassed').url(),
        },
        deferRender: true,
        rowId: 'id',
        language: window.dataTablesLocales(),
        paging: true,
        lengthChange: true,
        lengthMenu: [10, 25, 50, 100],
        pageLength: 50,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: true,
        scrollX: true,
        columns: [
            { data: 'date_reproduction', name: 'date_reproduction' },
            { data: 'name_dj', name: 'name_dj' },            
            { data: 'iframe_pc', name: 'iframe_pc' },
            { data: 'iframe_phone', name: 'iframe_phone' },
            {
                data: null, searchable: false, orderable: false, render: function (data, type, row) {
                    return "" +
                        "<div class=\"btn-group\" role=\"group\">" +
                        "<a href=\"" + route('sessions.edit', { id: data.id }) + "\">" +
                        "<button type=\"button\" class=\"btn btn-default btn-sm btn-outline\" data-toggle=\"tooltip\">\n" +
                        "<i class=\"fa fa-pen\"></i>" +
                        "</button>" +
                        "</a>" +
                        "<a class=\"backend-sessions-delete-btn\" data-id=\"" + data.id + "\">" +
                        "<button type=\"button\" data-id=\"" + data.id + "\" class=\"btn btn-default btn-sm btn-outline\" data-toggle=\"tooltip\">\n" +
                        "<i data-id=\"" + data.id + "\" class=\"fa fa-trash no-click\"></i>" +
                        "</button>" +
                        "</a>" +
                        "</div>";
                }
            }
        ],
    });

    $backendSessionsPassedDatatable.on('click', '.backend-sessions-delete-btn', function (e) {
        let id = jQ(e.target).attr('data-id');

        let locale = {
            'title': trans('backend.datatable.sweetalert.sessions.title'),
            'text': trans('backend.datatable.sweetalert.sessions.text'),
            'success': trans('backend.datatable.sweetalert.sessions.success'),
            'error': trans('backend.datatable.sweetalert.sessions.error')
        };

        window.deleteAlert(id, $backendSessionsPassedDatatable, 'sessions.destroy', locale);
    });
});
