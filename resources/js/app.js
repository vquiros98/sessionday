require("flatpickr/dist/flatpickr");
window.Calendar = require("@fullcalendar/core");
window.dayGridPlugin = require("@fullcalendar/daygrid");

require('./bootstrap');
require('./create.session');
require('./sessions.datatable');
require('./sessionsPassed.datatable');
require('./sessionsFullCalendar');

