jQ(document).ready(function(){
    jQ('.like__btn').on('click', function(){
        id = jQ('#session_id').val();
        var like = localStorage.getItem(id);
        createCookie('like', 'liked')
        if(like == null){
            localStorage.setItem(id, 'Liked');
            
            jQ.ajax({
                type:'GET',
                url: route('backend.sessions.addLike', id),

                success: function(response) {
                    console.log(response);                
                                
                },
                error: function(response){
                    console.log(response);
                    
                },
            });
        }else{
            // localStorage.removeItem(id);
        }
    })

    function createCookie(name,value,path) {
        var expires = "";
        var date = new Date();
        var midnight = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
        expires = "; expires=" + midnight.toGMTString();
        if (!path) {
            path = "/";
        }
        document.cookie = name + "=" + value + expires + "; path=" + path;
    }

})