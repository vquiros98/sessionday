jQ(document).ready(function () {

    jQ(document).scroll(function () {
        jQ("nav").css("background-color", "#FFFFFF");
        jQ(".letrasnav").css("color", "#000000");
        jQ("#letrasLogo").css("color", "#000000");
        jQ("nav").css("transition", "all 0.5s ease-in")

        if (jQ("nav").offset().top <= 0) {
            jQ("nav").css("background-color", "transparent");
            jQ("nav a").css("color", "white");
        }
    });

    jQ('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    jQ('.slider-nav').slick({
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
            },
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
            }
        ]
    });
});