@extends('pages.layout.master')
@section('styles')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e9231937966f10012731de4&product=inline-share-buttons' async='async'></script>

@section('principal')

@php
  use Jenssegers\Agent\Agent;
  $agent = new Agent();
@endphp

<div style="min-height: 870px;">
    <div id="home">
        <h1>SESSIONDAY</h1>
        <a type="button" href="#sessionday" class="btn" style="background-color: transparent; color: white; font-weight: 600; border: 2px solid #11B6A4; z-index: 1000">Listen Now</a>        
    </div>

    <div id="sessionday">
        <h2>Today's Se<span>ss</span>ion</h2>
        <div class="container">
            {{-- <div class="d-flex justify-content-end pb-2">
                <button class="like__btn animated">
                    <i class="like__icon fa fa-heart"></i>
                    <span class="like__number">{{$session->likes}}</span>
                </button>
            </div> --}}
        <input type="hidden" id="session_id" value="{{$session->id}}">
            @if ($agent->isMobile())
                {!!$session->iframe_phone!!}
            @else
                {!!$session->iframe_pc!!}
            @endif
        
            <div class="row pt-5 col-12">
                <div class="d-flex justify-content-center col-md-6 col-sm-12 pt-2">
                    <a href="https://twitter.com/intent/tweet?button_hashtag=SessionDay&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-size="large" data-text="Come enjoy the new session of the day in" data-related="SessionDay,SessionDay" data-show-count="false">Tweet #SessionDay</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                <div class="col-md-6 col-sm-12 pt-2">
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
                {{-- <div class="col-md-4 col-sm-12 d-flex justify-content-center pt-2">
                    <iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&layout=button&size=large&width=103&height=28&appId" width="103" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div> --}}
            </div>
        </div>
    </div>

    <div id="lastsessions">
        <h2>Last Se<span>ss</span>ions</h2>
        <div class="d-flex justify-content-center pb-5 ">

<!--Carousel Wrapper-->
            <div id="carousel-example-2" class="w-100 carousel slide carousel-fade" data-ride="carousel">
                <!--Indicators-->
                <ol class="carousel-indicators">
                <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-2" data-slide-to="1"></li>
                <li data-target="#carousel-example-2" data-slide-to="2"></li>
                </ol>
                <!--/.Indicators-->
                <!--Slides-->
                <div class="carousel-inner" role="listbox" style="height:600px">
                    <div class="carousel-item active">
                        <div class="view1 views">
                            <div class="backdrop"></div>
                            <div class="mask rgba-black-light"></div>
                        </div>
                        <div class="carousel-caption">
                            <h2 style="color:white" class="h3-responsive">Electronic</h2>
                            <p><b>Enjoy all this genders in the last week sessions</b></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!--Mask color-->
                        <div class="view2">
                            <div class="backdrop"></div>
                            <div class="mask rgba-black-strong"></div>
                        </div>
                        <div class="carousel-caption">
                            <h2 style="color:white" class="h3-responsive">R&B</h2>
                            <p><b>Enjoy all this genders in the last week sessions</b></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!--Mask color-->
                        <div class="view3">
                            <div class="backdrop"></div>
                            <div class="mask rgba-black-slight"></div>
                        </div>
                        <div class="carousel-caption">
                            <h2 style="color:white" class="h3-responsive">Reggaeton</h2>
                            <p><b>Enjoy all this genders in the last week sessions</b></p>
                        </div>
                    </div>
                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
            <!--/.Carousel Wrapper-->
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{route('lastSessions')}}">
                <button class="btn" style="background-color: transparent; color: black; font-weight: 600; border: 2px solid #000000; width: 300px; padding:15px; font-size: 1rem">SEE ALL SESSIONS OF THE WEEK</button>
            </a>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="sessionDayTwitter">
                <h2>Tweets of Se<span>ss</span>ionDay</h2>
                {{-- <div class="col-12 d-flex justify-content-center">
                    <img src="{{asset("images/imagenes/sessionDayTwitter.png")}}" class="img-fluid" width="600" height="150" alt="sessionDayTwitter">
                </div> --}}
                
                <div class="col-12 d-flex justify-content-center">
                    <a class="twitter-timeline" data-width="600" data-height="650" href="https://twitter.com/SessionDay"></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
        {{-- <div class="col-sm-12 col-md-6">
            <div id="sessionDayTwitter">
                <div id="tituloResume">
                    <h2><span>MY  </span>INSTAGRAM</h2>
                </div>
                <div style="text-align: center">
                    <div class="row" id="instagram" style="background-color:#FFFFFF; display:flex; flex-wrap: wrap; justify-content:center"></div>
                </div>

                <div id="instaCard" class="row pt-4" style="">
                    <div class="col-sm-12">
                        <div class="card d-flex justify-content-center text-align-center" style="transition: box-shadow .25s, -webkit-box-shadow .25s;border-radius: 2px;">
                            <div class="card-title d-flex justify-content-center text-align-center">
                                <a id="username" style="color: black;" href="https://www.instagram.com/sessiondayofficial/?hl=es" target="self"></a>
                                <a href="https://www.instagram.com/sessiondayofficial/?hl=es" target="self" class="btn btn-small" style="background-color: #3897f0; border-radius: 3px; color: #fff; font-weight: 600; align-items:flex-end">See more</a>
                            </div>
                            <div id="showImg" class="d-flex justify-content-center text-align-center"></div>
                            <div class="card-title">
                                <p id="likes"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

    </div>

    {{-- <div class="row">
        <div class="col-md-6 col-sm-12" id="about">
            <h1>About us</h1>
            <button class="btn" style="background-color: transparent; color:white; border-color: white;">Know more</button>
        </div>
        <div class="col-md-6 col-sm-12" id="contact">
            <h1>Contact us</h1>
            <button class="btn" style="background-color: transparent; color:white; border-color: white;">Contact</button>
        </div>
    </div> --}}
</div>
@endsection