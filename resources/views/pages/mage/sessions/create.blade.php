@extends('mage::layout.page')
@section('web-title', __('backend.sessions.create.web-title'))
@section('page-title', __('backend.sessions.create.page-title'))
@section('breadcrumbs')
<li class="breadcrumb-item">@lang('backend.sessions.index.breadcrumb.title')</li>
<li class="breadcrumb-item active">@lang('backend.sessions.create.breadcrumb.title')</li>
@endsection

@section('page')
    <form action="{{route('sessions.store')}}" class="add_product" method="POST" enctype="multipart/form-data">
        @method('POST')
            @include('pages.mage.sessions.forms.main')
    </form>
@endsection
