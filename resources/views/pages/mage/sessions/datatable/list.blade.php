<table id="backend-sessions-datatable" class="table table-bordered table-hover display" style="width:100%">
    <thead>
        <tr>
            <th>@lang('backend.sessions.datatable.field.date_reproduction')</th>
            <th>@lang('backend.sessions.datatable.field.name_dj')</th>            
            <th>@lang('backend.sessions.datatable.field.iframe_pc')</th>
            <th>@lang('backend.sessions.datatable.field.iframe_mobile')</th>            
            <th>@lang('backend.sessions.datatable.field.actions')</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
