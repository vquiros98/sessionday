@extends('mage::layout.page')
@section('web-title', __('backend.sessions.index.web-title'))
@section('page-title', __('backend.sessions.index.page-title'))
@section('breadcrumbs')
    <li class="breadcrumb-item active">@lang('backend.sessions.index.breadcrumb.title')</li>
@endsection

@section('page')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">@lang('backend.sessions.index.title')</h3>
            <div class="card-tools">
                <div class="input-group input-group-sm">
                    <a href="{{ route('sessions.create') }}">
                        <button class="btn btn-sm btn-primary">@lang('mage.users.new')</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('pages.mage.sessions.datatable.list')
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">@lang('backend.sessions.index.titlePassed')</h3>
        </div>
        <div class="card-body">
            @include('pages.mage.sessions.datatable.listPassed')
        </div>
    </div>
@endsection
