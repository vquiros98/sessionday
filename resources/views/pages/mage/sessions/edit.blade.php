@extends('mage::layout.page')
@section('web-title', __('backend.sessions.edit.web-title'))
@section('page-title', __('backend.sessions.edit.page-title'))
@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{route('sessions.index')}}">@lang('backend.sessions.index.breadcrumb.title')</a></li>
<li class="breadcrumb-item active">@lang('mage.users.edit.breadcrumb.title')</li>
@endsection

@section('page')

    <h3 class="mb-5 text-bold">@lang('sessions.edit.title')</h3>
    <form action="{{route('sessions.update', [$session->id])}}" method="POST">
        @method('PUT')
        @include('pages.mage.sessions.forms.main')
    </form>

@endsection