<div class="container">
    <div class="card">
        <div class="card-header">
            <h5 class="text-bold mb-0">@lang('backend.sessions.form.session_data')</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-12 col-md-6">
                    <label for="name_dj"><strong>@lang('backend.sessions.form.name_dj')</strong></label>
                    <input name="name_dj" type="text" class="form-control @if($errors->has('name_dj')) is-invalid @endif" id="name_dj" placeholder="@lang('backend.sessions.form.name_dj.placeholder')"
                    value="{{ old('name_dj', $session['name_dj'] ?? '') }}">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="date_reproduction"><strong>@lang('backend.sessions.form.date_reproduction')</strong></label>
                    <input name="date_reproduction" type="text" class="form-control date_session @if($errors->has('date_reproduction')) is-invalid @endif" id="date_reproduction" placeholder="@lang('backend.sessions.form.date_reproduction.placeholder')"
                    value="{{ old('date_reproduction', $session['date_reproduction'] ?? '') }}">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="iframe_pc"><strong>@lang('backend.sessions.form.iframe_pc')</strong></label>
                    <input name="iframe_pc" type="text" class="form-control @if($errors->has('iframe_pc')) is-invalid @endif" id="iframe_pc" placeholder="@lang('backend.sessions.form.iframe_pc.placeholder')"
                    value="{{ old('iframe_pc', $session['iframe_pc'] ?? '') }}">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="iframe_phone"><strong>@lang('backend.sessions.form.iframe_phone')</strong></label>
                    <input name="iframe_phone" type="text" class="form-control @if($errors->has('iframe_phone')) is-invalid @endif" id="iframe_phone" placeholder="@lang('backend.sessions.form.iframe_phone.placeholder')"
                    value="{{ old('iframe_phone', $session['iframe_phone'] ?? '') }}">
                </div>
            </div>
        </div>
    </div>

</div>
<div class="d-flex justify-content-center">
    {{ csrf_field() }}
    <button type="submit" class="btn btn-primary" style="width:450px; height:50px">@lang('backend.sessions.form.save')</button>
</div>
