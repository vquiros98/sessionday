<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="SessionDay - Discover the current sessions and the new artists of the moment" />

  <title>{{ env('APP_NAME')}} @yield('web-title')</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @laravelPWA
  @include('mage::components.favicon')
  @section('styles')      
      <link rel="stylesheet" href="{{ mix('/mage.css', '/vendor/mage') }}">
      <link rel="stylesheet" href="{{ mix('css/front.css') }}">

      @include('mage::assets.styles')
      <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
      <link href="https://sf.abarba.me/SF-UI-Display-Black.otf" rel="stylesheet">      
  @show

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166033004-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-166033004-1');
  </script>
</head>
<body style="font-family: 
'SF Display', 'Poppins', 'Merriweather Sans', sans-serif, Verdana, Geneva, Tahoma; background-color: #FFFFFF">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a id="letrasLogo" class="navbar-brand js-scroll-trigger" href="{{route('index')}}">SessionDay</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          <li class="nav-item">
            <a class="letrasnav nav-link js-scroll-trigger" style="color:white" href="#sessionday">Today</a>
          </li>
          <li class="nav-item">
            <a class="letrasnav nav-link js-scroll-trigger" style="color:white" href="#lastsessions">Last sessions</a>
          </li>
          {{-- <li class="nav-item">
            <a class="letrasnav nav-link js-scroll-trigger" style="color:white" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="letrasnav nav-link js-scroll-trigger" style="color:white" href="#contact">Contact</a>
          </li> --}}
        </ul>
      </div>
    </div>
  </nav>

  @yield('principal')

  <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright © 2019 - SessionDay</div>
    </div>
  </footer>

  <script>

  </script>

  @section('scripts')
    @routes
    @translations    
    <script src="{{ mix('/mage.js', '/vendor/mage') }}"></script>
    <script src="{{mix('js/front.js')}}"></script>
    @include('mage::assets.scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    @show    
</body>
</html>