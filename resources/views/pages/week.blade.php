@extends('pages.layout.master')
@section('styles')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e9231937966f10012731de4&product=inline-share-buttons' async='async'></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

@section('principal')
@php
  use Jenssegers\Agent\Agent;
  $agent = new Agent();
@endphp
<div style="min-height: 870px;">
  <div id="thisWeek">
    <h1>LAST WEEK</h1>
    <button class="btn" style="background-color: transparent; color: white; font-weight: 600; border: 2px solid #11B6A4; z-index: 1000">Listen Now</button>    
  </div>
  <div class="container pt-5 pb-5">    
    <div class="main">

      <div class="slider slider-nav d-flex justify-content-center">
        <div class="days"><h3>Today</h3></div>
        <div class="days"><h3>{{$week[1]}}</h3></div>
        <div class="days"><h3>{{$week[2]}}</h3></div>
        <div class="days"><h3>{{$week[3]}}</h3></div>
        <div class="days"><h3>{{$week[4]}}</h3></div>
        <div class="days"><h3>{{$week[5]}}</h3></div>
        <div class="days"><h3>{{$week[6]}}</h3></div>
      </div>
     
      <div width="100%" class="slider slider-for pt-5 carSessions">
        @foreach ($sessions as $session)
        <div class="container">
          @if ($agent->isMobile())
            {!!$session->iframe_phone!!}
          @else
            {!!$session->iframe_pc!!}
          @endif
      
          <div class="row pt-5 col-12">
              <div class="d-flex justify-content-center col-md-6 col-sm-12 pt-2">
                  <a href="https://twitter.com/intent/tweet?button_hashtag=SessionDay&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-size="large" data-text="Come enjoy the new session of the day in" data-related="SessionDay,SessionDay" data-show-count="false">Tweet #SessionDay</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
              </div>
              <div class="col-md-6 col-sm-12 pt-2">
                  <div class="sharethis-inline-share-buttons"></div>
              </div>
              {{-- <div class="col-md-4 col-sm-12 d-flex justify-content-center pt-2">
                  <iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&layout=button&size=large&width=103&height=28&appId" width="103" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
              </div> --}}
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection