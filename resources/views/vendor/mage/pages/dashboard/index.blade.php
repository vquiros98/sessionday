@extends('mage::layout.page')
@section('web-title', __('mage.dashboard.index.web-title'))
@section('page-title', __('mage.dashboard.index.page-title'))
@section('breadcrumbs')
    <li class="breadcrumb-item active">@lang('mage.dashboard.index.breadcrumb.title')</li>
@endsection

@section('page')
@if(auth()->guard('mage')->user()->hasRole('God'))
    <div class="card">
        {{-- <div class="card-header">
            <h3 class="card-title">@lang('mage.dashboard.index.title')</h3>
            <div class="card-tools"></div>
        </div> --}}
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-8" id="calendar"></div>
                <div id="sessionday_sessions_calendar_info" class="col-sm-12 col-md-4"
                    style="display: flex; align-items: center; text-align:justify; justify-content: center; flex-direction: column; vertical-align: middle">
                    <h3>Session info</h3>
                    <br>
                    <div class="iframeShow"></div>
                    <br>
                    <h4>SessionDay</h4>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
